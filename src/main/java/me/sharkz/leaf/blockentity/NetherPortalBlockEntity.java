package me.sharkz.leaf.blockentity;

import net.minestom.server.instance.block.states.NetherPortal;
import net.minestom.server.utils.BlockPosition;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class NetherPortalBlockEntity extends BlockEntity {

    public NetherPortalBlockEntity(BlockPosition position) {
        super(position);
    }

    public void setRelatedPortal(NetherPortal portal) {
        set("portal", portal, NetherPortal.class);
    }

    public NetherPortal getRelatedPortal() {
        return get("portal");
    }

}