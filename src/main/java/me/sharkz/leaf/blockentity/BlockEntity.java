package me.sharkz.leaf.blockentity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minestom.server.data.SerializableDataImpl;
import net.minestom.server.utils.BlockPosition;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@RequiredArgsConstructor
@Getter
public class BlockEntity extends SerializableDataImpl {

    private final BlockPosition position;

}