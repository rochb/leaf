package me.sharkz.leaf.blocks;

import me.sharkz.leaf.Leaf;
import me.sharkz.leaf.utils.ObjectInjector;
import net.minestom.server.MinecraftServer;
import net.minestom.server.event.player.PlayerBlockPlaceEvent;
import net.minestom.server.instance.block.Block;
import net.minestom.server.instance.block.BlockManager;
import net.minestom.server.instance.block.CustomBlock;
import net.minestom.server.network.ConnectionManager;

import java.util.List;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class LeafBlockManager extends ObjectInjector<LeafBlock> {

    private final BlockManager manager;
    private final ConnectionManager connectionManager;

    public LeafBlockManager() {
        this.manager = MinecraftServer.getBlockManager();
        this.connectionManager = MinecraftServer.getConnectionManager();

        Leaf.getLogger().info("Trying to inject blocks...");
        long start = System.currentTimeMillis();

        /* Injection */
        List<LeafBlock> blocks = inject("me.sharkz.leaf.blocks", LeafBlock.class);
        blocks.forEach(this::register);

        Leaf.getLogger().info(blocks.size() + " blocks were been injected! (Took " + (System.currentTimeMillis() - start) + " ms)");
    }

    public void register(LeafBlock block) {
        connectionManager.addPlayerInitialization(player -> player.addEventCallback(PlayerBlockPlaceEvent.class, event -> {
            if (event.getBlockStateId() != block.getBaseBlockId()) return;
            event.setCancelled(true);
            block.onPlace(event.getPlayer().getInstance(), event.getBlockPosition(), event.getBlockData());
            short blockID = block.getVisualBlockForPlacement(event.getPlayer(), event.getHand(), event.getBlockPosition());
            event.setBlockStateId(blockID);
            event.setCustomBlockId(block.getCustomBlockId());
        }));
        manager.registerCustomBlock(block);
    }

    public Optional<CustomBlock> getByBlock(Block block) {
        return Optional.ofNullable(manager.getCustomBlock(block.getBlockId()));
    }
}
