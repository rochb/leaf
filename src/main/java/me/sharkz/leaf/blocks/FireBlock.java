package me.sharkz.leaf.blocks;

import me.sharkz.leaf.damage.DamageTypes;
import net.minestom.server.data.Data;
import net.minestom.server.entity.Entity;
import net.minestom.server.entity.LivingEntity;
import net.minestom.server.instance.Instance;
import net.minestom.server.instance.block.Block;
import net.minestom.server.utils.BlockPosition;
import net.minestom.server.utils.time.TimeUnit;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class FireBlock extends LeafBlock {

    public FireBlock() {
        super(Block.FIRE);
    }

    @Override
    protected BlockPropertyList createPropertyValues() {
        return new BlockPropertyList().intRange("age", 0, 15);
    }

    @Override
    public void handleContact(Instance instance, BlockPosition position, Entity touching) {
        if (!(touching instanceof LivingEntity) || !touching.isOnFire()) return;
        ((LivingEntity) touching).damage(DamageTypes.IN_FIRE.getType(), 1.0f);
        ((LivingEntity) touching).setFireForDuration(8000, TimeUnit.MILLISECOND);
    }

    @Override
    public void scheduledUpdate(Instance instance, BlockPosition position, Data blockData) {
    /*
        NetherPortal portal = NetherPortal.findPortalFrameFromFrameBlock(instance, position.copy());
        if(Objects.nonNull(portal) && portal.tryFillFrame(instance)) portal.register(instance);
     */
    }

    @Override
    public void onPlace(Instance instance, BlockPosition blockPosition, Data data) {
        instance.scheduleUpdate(0, TimeUnit.TICK, blockPosition);
    }
}