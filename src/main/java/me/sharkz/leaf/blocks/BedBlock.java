package me.sharkz.leaf.blocks;

import me.sharkz.leaf.utils.BlocksUtils;
import net.minestom.server.data.Data;
import net.minestom.server.data.DataImpl;
import net.minestom.server.entity.Entity;
import net.minestom.server.entity.Player;
import net.minestom.server.instance.Instance;
import net.minestom.server.instance.block.Block;
import net.minestom.server.network.packet.server.play.BlockEntityDataPacket;
import net.minestom.server.utils.BlockPosition;
import net.minestom.server.utils.Direction;

import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class BedBlock extends LeafBlock {

    public BedBlock(Block bedBlock) {
        super(bedBlock);
    }

    @Override
    protected BlockPropertyList createPropertyValues() {
        return new BlockPropertyList().facingProperty("facing").booleanProperty("occupied").property("part", "foot", "head");
    }

    @Override
    public void onPlace(Instance instance, BlockPosition blockPosition, Data data) {
    }

    @Override
    public boolean onInteract(Player player, Player.Hand hand, BlockPosition blockPosition, Data data) {
        Instance instance = player.getInstance();
        if (Objects.requireNonNull(instance).getDimensionType().isBedSafe()) {
            player.setPose(Entity.Pose.SLEEPING);
            player.teleport(blockPosition.toPosition().clone().add(0, 0.75, 0));
            // TODO: make player sleep
            // TODO: checks for mobs
            // TODO: check for day
            //if(instance.getDayTime() > 12541L && instance.getDayTime() < 23458L) {

            //}
            return true;
        } else {
            Data args = new DataImpl();
            // args.set(VanillaExplosion.IS_FLAMING_KEY, true, Boolean.TYPE);
            instance.explode(blockPosition.getX() + 0.5f, blockPosition.getY() + 0.5f, blockPosition.getZ() + 0.5f, 5f, data);
            return true;
        }
    }

    @Override
    public void onDestroy(Instance instance, BlockPosition blockPosition, Data data) {
        BlockState currentState = getBlockStates().getFromInstance(instance, blockPosition);
        boolean isFoot = "foot".equals(currentState.get("part"));
        Direction facing = Direction.valueOf(currentState.get("facing").toUpperCase());
        BlockPosition otherPartPosition = new BlockPosition(blockPosition.getX(), blockPosition.getY(), blockPosition.getZ());
        if (!isFoot) {
            facing = facing.opposite();
        }
        otherPartPosition.add(facing.normalX(), facing.normalY(), facing.normalZ());
        BlocksUtils.dropOnBreak(instance, blockPosition);
        instance.setBlock(otherPartPosition, Block.AIR);
    }
}