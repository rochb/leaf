package me.sharkz.leaf.math;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minestom.server.utils.BlockPosition;
import net.minestom.server.utils.Vector;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public final class RayCast {

    public static Result rayCastBlocks(Vector start, Vector direction, float maxDistance, float stepLength, Predicate<BlockPosition> shouldContinue, Consumer<BlockPosition> onBlockStep) {
        return rayCastBlocks(start.getX(), start.getY(), start.getZ(), direction.getX(), direction.getY(), direction.getZ(), maxDistance, stepLength, shouldContinue, onBlockStep);
    }

    public static Result rayCastBlocks(double startX, double startY, double startZ, double dirX, double dirY, double dirZ, double maxDistance, double stepLength, Predicate<BlockPosition> shouldContinue, Consumer<BlockPosition> onBlockStep) {
        Vector ray = new Vector(dirX, dirY, dirZ);
        ray.normalize().multiply(stepLength);

        Vector position = new Vector(startX, startY, startZ);
        BlockPosition blockPos = new BlockPosition(position);
        Set<BlockPosition> reachedPositions = new HashSet<>();

        Result hit = null;
        for (float step = 0f; step < maxDistance; step += stepLength) {
            blockPos.setX((int) Math.floor(position.getX()));
            blockPos.setY((int) Math.floor(position.getY()));
            blockPos.setZ((int) Math.floor(position.getZ()));

            if (blockPos.getY() < 0 || blockPos.getY() >= 255) { // out of bounds
                hit = new Result(position, HitType.OUT_OF_BOUNDS);
                break;
            }

            if (!shouldContinue.test(blockPos)) {
                hit = new Result(position, HitType.BLOCK);
                break;
            }

            if (!reachedPositions.contains(blockPos)) {
                reachedPositions.add(new BlockPosition(blockPos.getX(), blockPos.getY(), blockPos.getZ()));
                onBlockStep.accept(blockPos);
            }

            position.add(ray.getX(), ray.getY(), ray.getZ());
        }
        if (hit == null)
            return new Result(position, HitType.NONE);
        return hit;
    }

    @RequiredArgsConstructor
    @Getter
    public static class Result {

        private final Vector finalPosition;
        private final HitType hitType;

    }

    public enum HitType {
        ENTITY,
        BLOCK,
        OUT_OF_BOUNDS,
        NONE
    }

}