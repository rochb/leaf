package me.sharkz.leaf.system;

import lombok.Getter;
import me.sharkz.leaf.Leaf;
import me.sharkz.leaf.configuration.wrapper.Configurable;
import me.sharkz.leaf.configuration.wrapper.WrappedConfiguration;
import net.minestom.server.world.Difficulty;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class ServerSettings extends WrappedConfiguration {

    @Configurable
    private String hostname;

    @Configurable
    private int port;

    @Configurable(key = "optifine-support")
    private boolean optifineSupportEnabled;

    @Configurable(key = "online-mode")
    private boolean mojangAuthEnabled;

    @Configurable
    private String difficulty;

    @Configurable(key = "network-compression-threshold")
    private int networkCompressionThreshold;

    @Configurable(key = "chunk-view-distance")
    private int chunkViewDistance;

    @Configurable(key = "entity-view-distance")
    private int entityViewDistance;

    @Configurable(key = "group-packets")
    private boolean groupPacketsEnabled;

    @Configurable(key = "cache-packets")
    private boolean cachePacketsEnabled;

    @Configurable(key = "max-packet-size")
    private int maxPacketSize;

    @Configurable(key = "rate-limit")
    private int rateLimit;

    @Configurable(key = "motd")
    private String motd;

    @Configurable(key = "velocity")
    private boolean velocitySupportEnabled;

    @Configurable(key = "bungecoord")
    private boolean bungecoordSupportEnabled;

    @Configurable(key = "max-players")
    private int maxPlayers;

    @Configurable(key = "overworld-name")
    private String overWorldName;

    @Configurable(key = "nether-name")
    private String netherName;

    @Configurable(key = "end-name")
    private String endWorldName;

    @Configurable(key = "allow-nether")
    private boolean netherWorldEnabled;

    @Configurable(key = "allow-end")
    private boolean endWorldEnabled;

    public ServerSettings(Leaf leaf) {
        super(new File(leaf.getDataFolder(), "settings.yml"), Leaf.class.getClassLoader().getResource("settings.yml"));
    }

    public boolean isValid() {
        return Objects.nonNull(hostname) && StringUtils.isNotEmpty(hostname) && Objects.nonNull(difficulty)
                && StringUtils.isNotEmpty(difficulty) && isDifficultyValid() && rateLimit > 0
                && maxPacketSize > 0 && entityViewDistance > 0 && chunkViewDistance > 0 && networkCompressionThreshold > 0
                && Objects.nonNull(overWorldName) && StringUtils.isNotEmpty(overWorldName)
                && (Objects.nonNull(netherName) && StringUtils.isNotEmpty(netherName) || !netherWorldEnabled)
                && (Objects.nonNull(endWorldName) && StringUtils.isNotEmpty(endWorldName) || !endWorldEnabled);
    }

    private boolean isDifficultyValid() {
        try {
            Difficulty.valueOf(difficulty.toUpperCase());
        } catch (Exception ignored) {
            return false;
        }
        return true;
    }

    public boolean isMotdValid() {
        return Objects.nonNull(motd) && StringUtils.isNoneBlank(motd);
    }

    public Difficulty getDifficulty() {
        return Difficulty.valueOf(difficulty.toUpperCase());
    }
}