package me.sharkz.leaf.system.exceptions;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ServerStartException extends Exception {


    public ServerStartException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServerStartException(String message) {
        super(message);
    }

}
