package me.sharkz.leaf;

import lombok.Getter;
import me.sharkz.leaf.blocks.LeafBlockManager;
import me.sharkz.leaf.commands.LeafCommandManager;
import me.sharkz.leaf.configuration.wrapper.YamlFileLoadException;
import me.sharkz.leaf.instances.LeafWorldManager;
import me.sharkz.leaf.items.LeafItemManager;
import me.sharkz.leaf.registry.LeafBlockRegistryManager;
import me.sharkz.leaf.system.ServerSettings;
import me.sharkz.leaf.system.exceptions.ServerStartException;
import me.sharkz.leaf.utils.LeafColor;
import net.kyori.adventure.text.Component;
import net.minestom.server.MinecraftServer;
import net.minestom.server.entity.Player;
import net.minestom.server.event.GlobalEventHandler;
import net.minestom.server.event.player.PlayerLoginEvent;
import net.minestom.server.event.server.ServerListPingEvent;
import net.minestom.server.extras.MojangAuth;
import net.minestom.server.extras.PlacementRules;
import net.minestom.server.extras.bungee.BungeeCordProxy;
import net.minestom.server.extras.optifine.OptifineSupport;
import net.minestom.server.extras.velocity.VelocityProxy;
import net.minestom.server.ping.ResponseData;
import net.minestom.server.storage.StorageManager;
import net.minestom.server.storage.systems.FileStorageSystem;
import net.minestom.server.utils.Position;
import net.minestom.server.utils.time.TimeUnit;
import net.minestom.server.utils.time.UpdateOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.Paths;

@Getter
public final class Leaf {

    private static Leaf instance;
    private final static Logger logger = LoggerFactory.getLogger(Leaf.class);

    /* Settings */
    private ServerSettings serverSettings;

    /* Instances */
    private final LeafWorldManager instanceManager;

    /* Commands */
    private final LeafCommandManager commandManager;

    /* Blocks */
    private final LeafBlockManager blockManager;

    /* Items */
    private final LeafItemManager itemManager;

    /* Blocks Registries */
    private final LeafBlockRegistryManager registryManager;

    public Leaf() {
        instance = this;

        MinecraftServer server = MinecraftServer.init();
        MinecraftServer.setBrandName("LeafMC");

        /* Server Settings */
        try {
            this.serverSettings = loadServerSettings();
        } catch (Exception e) {
            e.printStackTrace();
        }

        /* Instances */
        this.instanceManager = new LeafWorldManager(serverSettings);

        /* Events */
        GlobalEventHandler globalEventHandler = MinecraftServer.getGlobalEventHandler();

        /* Blocks */
        this.blockManager = new LeafBlockManager();

        /* Items */
        this.itemManager = new LeafItemManager();

        /* Registries */
        registryManager = new LeafBlockRegistryManager(itemManager, blockManager);

        /* Commands */
        this.commandManager = new LeafCommandManager();

        /* Storage */
        StorageManager storageManager = MinecraftServer.getStorageManager();
        storageManager.defineDefaultStorageSystem(FileStorageSystem::new);

        /* Benchmark */
        MinecraftServer.getBenchmarkManager().enable(new UpdateOption(10 * 1000, TimeUnit.MILLISECOND));

        PlacementRules.init();

        server.start(serverSettings.getHostname(), serverSettings.getPort());
    }

    /**
     * Load server settings
     *
     * @return server settings
     */
    private ServerSettings loadServerSettings() throws Exception {
        ServerSettings serverSettings = new ServerSettings(this);
        try {
            logger.info("Loading server settings...");
            serverSettings.load();
            if (!serverSettings.isValid())
                throw new ServerStartException("Invalid server settings!");

            MinecraftServer.setDifficulty(serverSettings.getDifficulty());
            MinecraftServer.setCompressionThreshold(serverSettings.getNetworkCompressionThreshold());
            MinecraftServer.setChunkViewDistance(serverSettings.getChunkViewDistance());
            MinecraftServer.setEntityViewDistance(serverSettings.getEntityViewDistance());
            MinecraftServer.setGroupedPacket(serverSettings.isGroupPacketsEnabled());
            MinecraftServer.setPacketCaching(serverSettings.isCachePacketsEnabled());
            MinecraftServer.setMaxPacketSize(serverSettings.getMaxPacketSize());
            MinecraftServer.setRateLimit(serverSettings.getRateLimit());

            if (serverSettings.isOptifineSupportEnabled())
                OptifineSupport.enable();
            if (serverSettings.isMojangAuthEnabled())
                MojangAuth.init();
            if (serverSettings.isBungecoordSupportEnabled())
                BungeeCordProxy.enable();
            if (serverSettings.isVelocitySupportEnabled())
                VelocityProxy.enable("rBaJB59W4AVI");

            MinecraftServer.getGlobalEventHandler().addEventCallback(ServerListPingEvent.class, e -> {
                ResponseData responseData = e.getResponseData();
                if (serverSettings.getMaxPlayers() >= 0)
                    responseData.setMaxPlayer(serverSettings.getMaxPlayers());
                else
                    responseData.setMaxPlayer(20);
                responseData.setOnline(MinecraftServer.getConnectionManager().getOnlinePlayers().size());
                if (serverSettings.isMotdValid())
                    responseData.setDescription(Component.text(LeafColor.translate(serverSettings.getMotd())));

                // TODO: remove followings
                responseData.setVersion("Leaf");
                responseData.setName("Leaf MC");
            });
            logger.info("Server settings loaded successfully!");
        } catch (YamlFileLoadException e) {
            MinecraftServer.stopCleanly();
            throw new ServerStartException("Couldn't load server settings!", e);
        }
        return serverSettings;
    }

    /**
     * Get current working directory
     *
     * @return data folder
     */
    public File getDataFolder() {
        return Paths.get("").toAbsolutePath().toFile();
    }

    public static Logger getLogger() {
        return logger;
    }

    public static Leaf get() {
        return instance;
    }
}
