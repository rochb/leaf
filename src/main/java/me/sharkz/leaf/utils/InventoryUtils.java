package me.sharkz.leaf.utils;

import net.minestom.server.entity.Player;
import net.minestom.server.item.ItemStack;
import net.minestom.server.item.StackingRule;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class InventoryUtils {

    public static void consumeItemIfNotCreative(Player player, ItemStack itemStack, Player.Hand hand) {
        if (player.isCreative()) return;
        StackingRule stackingRule = itemStack.getStackingRule();
        ItemStack newUsedItem = stackingRule.apply(itemStack, stackingRule.getAmount(itemStack) - 1);
        if (hand == Player.Hand.OFF)
            player.getInventory().setItemInOffHand(newUsedItem);
        else
            player.getInventory().setItemInMainHand(newUsedItem);

    }

}
