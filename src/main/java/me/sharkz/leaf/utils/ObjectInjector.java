package me.sharkz.leaf.utils;

import org.reflections.Reflections;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public abstract class ObjectInjector<T> {

    public List<T> inject(String path, Class<?> primitiveType) {
        return new HashSet<>(new Reflections(path)
                .getSubTypesOf(primitiveType))
                .stream()
                .map(aClass -> {
                    try {
                        return (T) aClass.newInstance();
                    } catch (InstantiationException | IllegalAccessException e) {
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }
}
