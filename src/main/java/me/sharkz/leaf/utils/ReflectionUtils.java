package me.sharkz.leaf.utils;

import java.lang.reflect.ParameterizedType;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ReflectionUtils {

    public static Class<?> findSuperClassParameterType(Object instance, int parameterIndex) {
        Class<?> subClass = instance.getClass();
        while (subClass != subClass.getSuperclass()) {
            // instance.getClass() is no subclass of classOfInterest or instance is a direct instance of classOfInterest
            subClass = subClass.getSuperclass();
            if (subClass == null) throw new IllegalArgumentException();
        }
        ParameterizedType parameterizedType = (ParameterizedType) subClass.getGenericSuperclass();
        return (Class<?>) parameterizedType.getActualTypeArguments()[parameterIndex];
    }
}
