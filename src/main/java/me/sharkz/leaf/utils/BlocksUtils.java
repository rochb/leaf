package me.sharkz.leaf.utils;

import net.minestom.server.MinecraftServer;
import net.minestom.server.data.Data;
import net.minestom.server.data.DataImpl;
import net.minestom.server.entity.ItemEntity;
import net.minestom.server.gamedata.loottables.LootTable;
import net.minestom.server.gamedata.loottables.LootTableManager;
import net.minestom.server.instance.Instance;
import net.minestom.server.instance.block.Block;
import net.minestom.server.instance.block.CustomBlock;
import net.minestom.server.item.ItemStack;
import net.minestom.server.utils.BlockPosition;
import net.minestom.server.utils.NamespaceID;
import net.minestom.server.utils.Position;
import net.minestom.server.utils.time.TimeUnit;

import java.io.FileNotFoundException;
import java.util.List;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class BlocksUtils {

    public static void dropOnBreak(Instance instance, BlockPosition position) {
        LootTable table = null;
        LootTableManager lootTableManager = MinecraftServer.getLootTableManager();
        CustomBlock customBlock = instance.getCustomBlock(position);
        if (customBlock != null)
            table = customBlock.getLootTable(lootTableManager);
        Block block = Block.fromStateId(instance.getBlockStateId(position));
        Data lootTableArguments = new DataImpl();
        // TODO: tool used, silk touch, etc.
        try {
            if (table == null)
                table = lootTableManager.load(NamespaceID.from("blocks/" + block.name().toLowerCase()));
            List<ItemStack> stacks = table.generate(lootTableArguments);
            for (ItemStack item : stacks) {
                Position spawnPosition = new Position((float) (position.getX() + 0.2f + Math.random() * 0.6f), (float) (position.getY() + 0.5f), (float) (position.getZ() + 0.2f + Math.random() * 0.6f));
                ItemEntity itemEntity = new ItemEntity(item, spawnPosition);

                itemEntity.getVelocity().setX((float) (Math.random() * 2f - 1f));
                itemEntity.getVelocity().setY((float) (Math.random() * 2f));
                itemEntity.getVelocity().setZ((float) (Math.random() * 2f - 1f));

                itemEntity.setPickupDelay(500, TimeUnit.MILLISECOND);
                itemEntity.setInstance(instance);
            }
        } catch (FileNotFoundException e) {
            // ignore missing table
        }
    }

}
