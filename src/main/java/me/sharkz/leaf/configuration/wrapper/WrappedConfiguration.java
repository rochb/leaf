package me.sharkz.leaf.configuration.wrapper;

import me.sharkz.leaf.configuration.ConfigurationSection;

import java.io.File;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.Objects;

/**
 * Represents a dynamically configurable
 * wrapper for a configuration file on the disk.
 *
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public abstract class WrappedConfiguration extends FileWrapper {

    /**
     * Constructs the configurable file wrapper.
     *
     * @param file file object
     */
    public WrappedConfiguration(File file) {
        super(file);
    }

    /**
     * Constructs the configurable file wrapper with a default resource.
     *
     * @param file            file object
     * @param defaultResource default resource URL
     */
    public WrappedConfiguration(File file, URL defaultResource) {
        super(file, defaultResource);
    }

    /**
     * Loads the configuration file from the disk.
     * Fills attributes of the instance with values from the file.
     *
     * @throws YamlFileLoadException thrown if the configuration file cannot be loaded
     */
    @Override
    public void load() throws YamlFileLoadException {
        super.load();

        for (Field field : this.getClass().getDeclaredFields()) {
            Configurable conf = field.getAnnotation(Configurable.class);
            if (Objects.isNull(conf)) continue;

            String configKey = (conf.key().isEmpty()) ? field.getName() : conf.key();
            Object value = null;
            if (configuration.isConfigurationSection(configKey)) {
                ConfigurationSection section = configuration.getConfigurationSection(configKey);
                if (field.getType().equals(ConfigurationSection.class))
                    value = section;
            }
            if (Objects.isNull(value)) value = this.configuration.get(configKey);

            try {
                field.setAccessible(true);
                field.set(this, value);
                field.setAccessible(false);
            } catch (ReflectiveOperationException | IllegalArgumentException e) {
                String configName = getClass().getSimpleName().toLowerCase();
                throw new YamlFileLoadException(String.format(
                        "Cannot set the config value %s of key %s for %s",
                        value, configKey, configName
                ), e);
            }
        }
    }

}
