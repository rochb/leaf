package me.sharkz.leaf.configuration.temporary;

import me.sharkz.leaf.configuration.Configuration;
import me.sharkz.leaf.configuration.ConfigurationSection;
import org.apache.commons.lang3.Validate;

import java.util.Map;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class TemporaryConfiguration extends TemporarySection implements Configuration {

    protected Configuration defaults;
    protected TemporaryConfigurationOptions options;

    /**
     * Creates an empty {@link TemporaryConfiguration} with no default values.
     */
    public TemporaryConfiguration() {}

    /**
     * Creates an empty {@link TemporaryConfiguration} using the specified {@link
     * Configuration} as a source for all default values.
     *
     * @param defaults Default value provider
     * @throws IllegalArgumentException Thrown if defaults is null
     */
    public TemporaryConfiguration(Configuration defaults) {
        this.defaults = defaults;
    }

    @Override
    public void addDefault(String path, Object value) {
        Validate.notNull(path, "Path may not be null");

        if (defaults == null)
            defaults = new TemporaryConfiguration();
        defaults.set(path, value);
    }

    public void addDefaults(Map<String, Object> defaults) {
        Validate.notNull(defaults, "Defaults may not be null");
        defaults.forEach(this::addDefault);
    }

    public void addDefaults(Configuration defaults) {
        Validate.notNull(defaults, "Defaults may not be null");
        addDefaults(defaults.getValues(true));
    }

    public void setDefaults(Configuration defaults) {
        Validate.notNull(defaults, "Defaults may not be null");
        this.defaults = defaults;
    }

    public Configuration getDefaults() {
        return defaults;
    }

    @Override
    public ConfigurationSection getParent() {
        return null;
    }

    public TemporaryConfigurationOptions options() {
        if (options == null)
            options = new TemporaryConfigurationOptions(this);
        return options;
    }
}
