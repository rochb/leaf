package me.sharkz.leaf.configuration.temporary;

import me.sharkz.leaf.configuration.ConfigurationOptions;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class TemporaryConfigurationOptions extends ConfigurationOptions {
    
    protected TemporaryConfigurationOptions(TemporaryConfiguration configuration) {
        super(configuration);
    }

    @Override
    public TemporaryConfiguration configuration() {
        return (TemporaryConfiguration) super.configuration();
    }

    @Override
    public TemporaryConfigurationOptions copyDefaults(boolean value) {
        super.copyDefaults(value);
        return this;
    }

    @Override
    public TemporaryConfigurationOptions pathSeparator(char value) {
        super.pathSeparator(value);
        return this;
    }
}
