package me.sharkz.leaf.configuration.temporary;

import me.sharkz.leaf.configuration.Configuration;
import me.sharkz.leaf.configuration.ConfigurationSection;
import net.minestom.server.color.Color;
import net.minestom.server.item.ItemStack;
import org.apache.commons.lang3.Validate;

import java.util.*;
import java.util.stream.Collectors;

import static me.sharkz.leaf.utils.NumberConversions.*;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class TemporarySection implements ConfigurationSection {

    protected final Map<String, Object> map = new LinkedHashMap<>();
    private final Configuration root;
    private final ConfigurationSection parent;
    private final String path;
    private final String fullPath;

    /**
     * Creates an empty TemporarySection for use as a root {@link Configuration}
     * section.
     * <p>
     * Note that calling this without being yourself a {@link Configuration}
     * will throw an exception!
     *
     * @throws IllegalStateException Thrown if this is not a {@link
     *                               Configuration} root.
     */
    protected TemporarySection() {
        if (!(this instanceof Configuration))
            throw new IllegalStateException("Cannot construct a root TemporarySection when not a Configuration");
        this.path = "";
        this.fullPath = "";
        this.parent = null;
        this.root = (Configuration) this;
    }

    /**
     * Creates an empty TemporarySection with the specified parent and path.
     *
     * @param parent Parent section that contains this own section.
     * @param path   Path that you may access this section from via the root
     *               {@link Configuration}.
     * @throws IllegalArgumentException Thrown is parent or path is null, or
     *                                  if parent contains no root Configuration.
     */
    protected TemporarySection(ConfigurationSection parent, String path) {
        Validate.notNull(parent, "Parent cannot be null");
        Validate.notNull(path, "Path cannot be null");

        this.path = path;
        this.parent = parent;
        this.root = parent.getRoot();

        Validate.notNull(root, "Path cannot be orphaned");

        this.fullPath = createPath(parent, path);
    }

    public Set<String> getKeys(boolean deep) {
        Set<String> result = new LinkedHashSet<>();

        Configuration root = getRoot();
        if (root != null && root.options().copyDefaults()) {
            ConfigurationSection defaults = getDefaultSection();
            if (defaults != null)
                result.addAll(defaults.getKeys(deep));
        }
        mapChildrenKeys(result, this, deep);

        return result;
    }

    public Map<String, Object> getValues(boolean deep) {
        Map<String, Object> result = new LinkedHashMap<>();

        Configuration root = getRoot();
        if (root != null && root.options().copyDefaults()) {
            ConfigurationSection defaults = getDefaultSection();

            if (defaults != null)
                result.putAll(defaults.getValues(deep));
        }
        mapChildrenValues(result, this, deep);

        return result;
    }

    public boolean contains(String path) {
        return get(path) != null;
    }

    public boolean isSet(String path) {
        Configuration root = getRoot();
        if (root == null)
            return false;
        if (root.options().copyDefaults())
            return contains(path);
        return get(path, null) != null;
    }

    public String getCurrentPath() {
        return fullPath;
    }

    public String getName() {
        return path;
    }

    public Configuration getRoot() {
        return root;
    }

    public ConfigurationSection getParent() {
        return parent;
    }

    public void addDefault(String path, Object value) {
        Validate.notNull(path, "Path cannot be null");

        Configuration root = getRoot();
        if (root == null)
            throw new IllegalStateException("Cannot add default without root");
        if (root == this)
            throw new UnsupportedOperationException("Unsupported addDefault(String, Object) implementation");
        root.addDefault(createPath(this, path), value);
    }

    public ConfigurationSection getDefaultSection() {
        Configuration root = getRoot();
        Configuration defaults = root == null ? null : root.getDefaults();

        if (defaults != null && defaults.isConfigurationSection(getCurrentPath()))
            return defaults.getConfigurationSection(getCurrentPath());

        return null;
    }

    public void set(String path, Object value) {
        Validate.notEmpty(path, "Cannot set to an empty path");

        Configuration root = getRoot();
        if (root == null)
            throw new IllegalStateException("Cannot use section without a root");

        final char separator = root.options().pathSeparator();
        int i1 = -1, i2;
        ConfigurationSection section = this;
        while ((i1 = path.indexOf(separator, i2 = i1 + 1)) != -1) {
            String node = path.substring(i2, i1);
            ConfigurationSection subSection = section.getConfigurationSection(node);
            if (subSection == null)
                section = section.createSection(node);
            else
                section = subSection;
        }

        String key = path.substring(i2);
        if (section == this) {
            if (value == null)
                map.remove(key);
            else
                map.put(key, value);
        } else
            section.set(key, value);
    }

    public Object get(String path) {
        return get(path, getDefault(path));
    }

    public Object get(String path, Object def) {
        Validate.notNull(path, "Path cannot be null");

        if (path.length() == 0) return this;

        Configuration root = getRoot();
        if (root == null) throw new IllegalStateException("Cannot access section without a root");


        final char separator = root.options().pathSeparator();
        // i1 is the leading (higher) index
        // i2 is the trailing (lower) index
        int i1 = -1, i2;
        ConfigurationSection section = this;
        while ((i1 = path.indexOf(separator, i2 = i1 + 1)) != -1) {
            section = section.getConfigurationSection(path.substring(i2, i1));
            if (section == null)
                return def;

        }

        String key = path.substring(i2);
        if (section == this) {
            Object result = map.get(key);
            return (result == null) ? def : result;
        }
        return section.get(key, def);
    }

    public ConfigurationSection createSection(String path) {
        Validate.notEmpty(path, "Cannot create section at empty path");
        Configuration root = getRoot();
        if (root == null)
            throw new IllegalStateException("Cannot create section without a root");


        final char separator = root.options().pathSeparator();
        // i1 is the leading (higher) index
        // i2 is the trailing (lower) index
        int i1 = -1, i2;
        ConfigurationSection section = this;
        while ((i1 = path.indexOf(separator, i2 = i1 + 1)) != -1) {
            String node = path.substring(i2, i1);
            ConfigurationSection subSection = section.getConfigurationSection(node);
            if (subSection == null)
                section = section.createSection(node);
            else
                section = subSection;

        }

        String key = path.substring(i2);
        if (section == this) {
            ConfigurationSection result = new TemporarySection(this, key);
            map.put(key, result);
            return result;
        }
        return section.createSection(key);
    }

    public ConfigurationSection createSection(String path, Map<?, ?> map) {
        ConfigurationSection section = createSection(path);
        map.forEach((key, value) -> {
            if (value instanceof Map)
                section.createSection(key.toString(), (Map<?, ?>) value);
            else
                section.set(key.toString(), value);
        });
        return section;
    }

    // Primitives
    public String getString(String path) {
        Object def = getDefault(path);
        return getString(path, def != null ? def.toString() : null);
    }

    public String getString(String path, String def) {
        Object val = get(path, def);
        return (val != null) ? val.toString() : def;
    }

    public boolean isString(String path) {
        Object val = get(path);
        return val instanceof String;
    }

    public int getInt(String path) {
        Object def = getDefault(path);
        return getInt(path, (def instanceof Number) ? toInt(def) : 0);
    }

    public int getInt(String path, int def) {
        Object val = get(path, def);
        return (val instanceof Number) ? toInt(val) : def;
    }

    public boolean isInt(String path) {
        Object val = get(path);
        return val instanceof Integer;
    }

    public boolean getBoolean(String path) {
        Object def = getDefault(path);
        return getBoolean(path, (def instanceof Boolean) ? (Boolean) def : false);
    }

    public boolean getBoolean(String path, boolean def) {
        Object val = get(path, def);
        return (val instanceof Boolean) ? (Boolean) val : def;
    }

    public boolean isBoolean(String path) {
        Object val = get(path);
        return val instanceof Boolean;
    }

    public double getDouble(String path) {
        Object def = getDefault(path);
        return getDouble(path, (def instanceof Number) ? toDouble(def) : 0);
    }

    public double getDouble(String path, double def) {
        Object val = get(path, def);
        return (val instanceof Number) ? toDouble(val) : def;
    }

    public boolean isDouble(String path) {
        Object val = get(path);
        return val instanceof Double;
    }

    public long getLong(String path) {
        Object def = getDefault(path);
        return getLong(path, (def instanceof Number) ? toLong(def) : 0);
    }

    public long getLong(String path, long def) {
        Object val = get(path, def);
        return (val instanceof Number) ? toLong(val) : def;
    }

    public boolean isLong(String path) {
        Object val = get(path);
        return val instanceof Long;
    }

    // Java
    public List<?> getList(String path) {
        Object def = getDefault(path);
        return getList(path, (def instanceof List) ? (List<?>) def : null);
    }

    public List<?> getList(String path, List<?> def) {
        Object val = get(path, def);
        return (List<?>) ((val instanceof List) ? val : def);
    }

    public boolean isList(String path) {
        Object val = get(path);
        return val instanceof List;
    }

    public List<String> getStringList(String path) {
        List<?> list = getList(path);
        if (list == null)
            return new ArrayList<>(0);
        return list.stream()
                .filter(o -> (o instanceof String) || (isPrimitiveWrapper(o)))
                .map(String::valueOf)
                .collect(Collectors.toList());
    }

    public List<Integer> getIntegerList(String path) {
        List<?> list = getList(path);
        if (list == null)
            return new ArrayList<>(0);

        return list.stream()
                .map(o -> {
                    if (o instanceof Integer)
                        return (Integer) o;
                    else if (o instanceof String) {
                        try {
                            return Integer.valueOf((String) o);
                        } catch (Exception ignored) {
                        }
                    } else if (o instanceof Character)
                        return (int) (Character) o;
                    else if (o instanceof Number)
                        return ((Number) o).intValue();
                    return null;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public List<Boolean> getBooleanList(String path) {
        List<?> list = getList(path);

        if (list == null)
            return new ArrayList<>(0);

        return list.stream()
                .map(o -> {
                    if (o instanceof Boolean)
                        return (Boolean) o;
                    else if (o instanceof String) {
                        if (Boolean.TRUE.toString().equals(o))
                            return true;
                        else if (Boolean.FALSE.toString().equals(o))
                            return false;
                    }
                    return null;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public List<Double> getDoubleList(String path) {
        List<?> list = getList(path);

        if (list == null)
            return new ArrayList<>(0);

        return list.stream()
                .map(o -> {
                    if (o instanceof Double)
                        return (Double) o;
                    else if (o instanceof String) {
                        try {
                            return Double.valueOf((String) o);
                        } catch (Exception ignored) {
                        }
                    } else if (o instanceof Character)
                        return (double) (Character) o;
                    else if (o instanceof Number)
                        return ((Number) o).doubleValue();
                    return null;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public List<Float> getFloatList(String path) {
        List<?> list = getList(path);

        if (list == null)
            return new ArrayList<>(0);

        return list.stream()
                .map(o -> {
                    if (o instanceof Float)
                        return (Float) o;
                    else if (o instanceof String) {
                        try {
                            return Float.valueOf((String) o);
                        } catch (Exception ignored) {
                        }
                    } else if (o instanceof Character)
                        return (float) (Character) o;
                    else if (o instanceof Number)
                        return ((Number) o).floatValue();
                    return null;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public List<Long> getLongList(String path) {
        List<?> list = getList(path);

        if (list == null)
            return new ArrayList<>(0);

        return list.stream()
                .map(o -> {
                    if (o instanceof Long)
                        return (Long) o;
                    else if (o instanceof String) {
                        try {
                            return Long.valueOf((String) o);
                        } catch (Exception ignored) {
                        }
                    } else if (o instanceof Character)
                        return (long) (Character) o;
                    else if (o instanceof Number)
                        return ((Number) o).longValue();
                    return null;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public List<Byte> getByteList(String path) {
        List<?> list = getList(path);

        if (list == null)
            return new ArrayList<>(0);

        return list.stream()
                .map(o -> {
                    if (o instanceof Byte)
                        return (Byte) o;
                    else if (o instanceof String) {
                        try {
                            return Byte.valueOf((String) o);
                        } catch (Exception ignored) {
                        }
                    } else if (o instanceof Character)
                        return (byte) ((Character) o).charValue();
                    else if (o instanceof Number)
                        return ((Number) o).byteValue();
                    return null;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public List<Character> getCharacterList(String path) {
        List<?> list = getList(path);

        if (list == null)
            return new ArrayList<>(0);

        return list.stream()
                .map(o -> {
                    if (o instanceof Character)
                        return (Character) o;
                    else if (o instanceof String) {
                        String str = (String) o;
                        if (str.length() == 1)
                            return str.charAt(0);
                    } else if (o instanceof Number)
                        return (char) ((Number) o).intValue();
                    return null;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public List<Short> getShortList(String path) {
        List<?> list = getList(path);

        if (list == null)
            return new ArrayList<>(0);

        return list.stream()
                .map(o -> {
                    if (o instanceof Short)
                        return (Short) o;
                    else if (o instanceof String) {
                        try {
                            return Short.valueOf((String) o);
                        } catch (Exception ignored) {
                        }
                    } else if (o instanceof Character)
                        return (short) ((Character) o).charValue();
                    else if (o instanceof Number)
                        return ((Number) o).shortValue();
                    return null;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public List<Map<?, ?>> getMapList(String path) {
        List<?> list = getList(path);

        if (list == null)
            return Collections.emptyList();

        return list.stream()
                .filter(o -> o instanceof Map)
                .map(o -> (Map<?, ?>) o)
                .collect(Collectors.toList());
    }

    // Bukkit
    public ItemStack getItemStack(String path) {
        Object def = getDefault(path);
        return getItemStack(path, (def instanceof ItemStack) ? (ItemStack) def : null);
    }

    public ItemStack getItemStack(String path, ItemStack def) {
        Object val = get(path, def);
        return (val instanceof ItemStack) ? (ItemStack) val : def;
    }

    public boolean isItemStack(String path) {
        Object val = get(path);
        return val instanceof ItemStack;
    }

    public Color getColor(String path) {
        Object def = getDefault(path);
        return getColor(path, (def instanceof Color) ? (Color) def : null);
    }

    @Override
    public Color getColor(String path, Color def) {
        Object val = get(path, def);
        return (val instanceof Color) ? (Color) val : def;
    }

    public boolean isColor(String path) {
        Object val = get(path);
        return val instanceof Color;
    }

    public ConfigurationSection getConfigurationSection(String path) {
        Object val = get(path, null);
        if (val != null)
            return (val instanceof ConfigurationSection) ? (ConfigurationSection) val : null;
        val = get(path, getDefault(path));
        return (val instanceof ConfigurationSection) ? createSection(path) : null;
    }

    public boolean isConfigurationSection(String path) {
        Object val = get(path);
        return val instanceof ConfigurationSection;
    }

    protected boolean isPrimitiveWrapper(Object input) {
        return input instanceof Integer || input instanceof Boolean ||
                input instanceof Character || input instanceof Byte ||
                input instanceof Short || input instanceof Double ||
                input instanceof Long || input instanceof Float;
    }

    protected Object getDefault(String path) {
        Validate.notNull(path, "Path cannot be null");

        Configuration root = getRoot();
        Configuration defaults = root == null ? null : root.getDefaults();
        return (defaults == null) ? null : defaults.get(createPath(this, path));
    }

    protected void mapChildrenKeys(Set<String> output, ConfigurationSection section, boolean deep) {
        if (!(section instanceof TemporarySection)) {
            section.getKeys(deep)
                    .forEach(s -> output.add(createPath(section, s, this)));
            return;
        }
        TemporarySection sec = (TemporarySection) section;

        for (Map.Entry<String, Object> entry : sec.map.entrySet()) {
            output.add(createPath(section, entry.getKey(), this));

            if (deep && entry.getValue() instanceof ConfigurationSection) {
                ConfigurationSection subsection = (ConfigurationSection) entry.getValue();
                mapChildrenKeys(output, subsection, true);
            }
        }
    }

    protected void mapChildrenValues(Map<String, Object> output, ConfigurationSection section, boolean deep) {
        if (!(section instanceof TemporarySection)) {
            section.getValues(deep).forEach((s, o) -> output.put(createPath(section, s, this), o));
            return;
        }
        TemporarySection sec = (TemporarySection) section;

        for (Map.Entry<String, Object> entry : sec.map.entrySet()) {
            output.put(createPath(section, entry.getKey(), this), entry.getValue());

            if (entry.getValue() instanceof ConfigurationSection && deep)
                mapChildrenValues(output, (ConfigurationSection) entry.getValue(), true);
        }
    }

    /**
     * Creates a full path to the given {@link ConfigurationSection} from its
     * root {@link Configuration}.
     * <p>
     * You may use this method for any given {@link ConfigurationSection}, not
     * only {@link TemporarySection}.
     *
     * @param section Section to create a path for.
     * @param key     Name of the specified section.
     * @return Full path of the section from its root.
     */
    public static String createPath(ConfigurationSection section, String key) {
        return createPath(section, key, (section == null) ? null : section.getRoot());
    }

    /**
     * Creates a relative path to the given {@link ConfigurationSection} from
     * the given relative section.
     * <p>
     * You may use this method for any given {@link ConfigurationSection}, not
     * only {@link TemporarySection}.
     *
     * @param section    Section to create a path for.
     * @param key        Name of the specified section.
     * @param relativeTo Section to create the path relative to.
     * @return Full path of the section from its root.
     */
    public static String createPath(ConfigurationSection section, String key, ConfigurationSection relativeTo) {
        Validate.notNull(section, "Cannot create path without a section");
        Configuration root = section.getRoot();
        if (root == null)
            throw new IllegalStateException("Cannot create path without a root");

        char separator = root.options().pathSeparator();

        StringBuilder builder = new StringBuilder();
        for (ConfigurationSection parent = section; (parent != null) && (parent != relativeTo); parent = parent.getParent()) {
            if (builder.length() > 0)
                builder.insert(0, separator);
            builder.insert(0, parent.getName());
        }

        if ((key != null) && (key.length() > 0)) {
            if (builder.length() > 0)
                builder.append(separator);
            builder.append(key);
        }

        return builder.toString();
    }

    @Override
    public String toString() {
        Configuration root = getRoot();
        return getClass().getSimpleName() +
                "[path='" +
                getCurrentPath() +
                "', root='" +
                (root == null ? null : root.getClass().getSimpleName()) +
                "']";
    }
}
