package me.sharkz.leaf.instances;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.minestom.server.instance.InstanceContainer;
import net.minestom.server.utils.Position;

import java.util.Random;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
@Setter
@AllArgsConstructor
public class LeafWorld {

    private final String name;
    private final long seed;
    private final InstanceContainer container;

    private Position spawn;

    public LeafWorld(String name, InstanceContainer container) {
        this.name = name;
        this.container = container;
        this.seed = new Random(System.currentTimeMillis()).nextLong();
        this.spawn = new Position(0, 70, 0);
    }
}
