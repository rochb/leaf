package me.sharkz.leaf.instances.dimensions;

import lombok.Getter;
import me.sharkz.leaf.io.MinecraftData;
import net.minestom.server.MinecraftServer;
import net.minestom.server.utils.NamespaceID;
import net.minestom.server.world.DimensionType;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public enum LeafDimensionType {
    OVERWORLD("minecraft:overworld", DimensionType.builder(NamespaceID.from("minecraft:overworld"))
            .ultrawarm(false)
            .natural(true)
            .piglinSafe(false)
            .respawnAnchorSafe(false)
            .bedSafe(true)
            .raidCapable(true)
            .skylightEnabled(true)
            .ceilingEnabled(false)
            .fixedTime(null)
            .ambientLight(0.0f)
            .logicalHeight(256)
            .infiniburn(NamespaceID.from("minecraft:infiniburn_overworld"))),

    NETHER("minecraft:the_nether", DimensionType.builder(NamespaceID.from("minecraft:the_nether"))
            .ultrawarm(true)
            .natural(true)
            .piglinSafe(true)
            .respawnAnchorSafe(false)
            .bedSafe(true)
            .raidCapable(true)
            .skylightEnabled(false)
            .ceilingEnabled(false)
            .fixedTime(null)
            .ambientLight(0.0f)
            .logicalHeight(256)
            .infiniburn(NamespaceID.from("minecraft:infiniburn_the_nether"))),

    END("minecraft:the_end", DimensionType.builder(NamespaceID.from("minecraft:the_end"))
            .ultrawarm(true)
            .natural(true)
            .piglinSafe(false)
            .respawnAnchorSafe(false)
            .bedSafe(false)
            .raidCapable(true)
            .skylightEnabled(false)
            .ceilingEnabled(false)
            .fixedTime(null)
            .ambientLight(0.0f)
            .logicalHeight(256)
            .infiniburn(NamespaceID.from("minecraft:infiniburn_the_end")));

    private final DimensionType dimension;

    LeafDimensionType(String identifier, DimensionType.DimensionTypeBuilder builder) {
        if (!MinecraftServer.getDimensionTypeManager().isRegistered(NamespaceID.from(identifier)))
            MinecraftServer.getDimensionTypeManager().addDimension(builder.build());
        this.dimension = MinecraftData.open(NamespaceID.from(identifier), "dimension_type", DimensionType.class).orElseThrow();
    }
}
