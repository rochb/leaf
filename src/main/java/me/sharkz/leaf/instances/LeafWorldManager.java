package me.sharkz.leaf.instances;

import lombok.Getter;
import me.sharkz.leaf.Leaf;
import me.sharkz.leaf.instances.anvil.FileSystemStorage;
import me.sharkz.leaf.instances.dimensions.LeafDimensionType;
import me.sharkz.leaf.instances.generators.LeafChunkGenerator;
import me.sharkz.leaf.instances.generators.NoiseTestGenerator;
import me.sharkz.leaf.system.ServerSettings;
import net.minestom.server.MinecraftServer;
import net.minestom.server.data.SerializableDataImpl;
import net.minestom.server.event.player.PlayerLoginEvent;
import net.minestom.server.instance.ExplosionSupplier;
import net.minestom.server.instance.InstanceContainer;
import net.minestom.server.instance.InstanceManager;
import net.minestom.server.storage.StorageManager;
import net.minestom.server.world.DimensionType;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class LeafWorldManager {

    @Getter
    private final List<LeafWorld> worlds = new ArrayList<>();
    private final NoiseTestGenerator noiseTestGenerator = new NoiseTestGenerator();

    private final InstanceManager manager;
    private final StorageManager storageManager;
    private final ExplosionSupplier explosionSupplier;

    public LeafWorldManager(ServerSettings serverSettings) {
        this.manager = MinecraftServer.getInstanceManager();
        this.storageManager = MinecraftServer.getStorageManager();

        this.explosionSupplier = (centerX, centerY, centerZ, strength, additionalData) -> {
            boolean isTNT = additionalData != null ? additionalData.getOrDefault(LeafExplosion.DROP_EVERYTHING_KEY, false) : false;
            boolean noBlockDamage = additionalData != null ? additionalData.getOrDefault(LeafExplosion.DONT_DESTROY_BLOCKS_KEY, false) : false;
            return new LeafExplosion(centerX, centerY, centerZ, strength, false, isTNT, !noBlockDamage);
        };
        this.storageManager.defineDefaultStorageSystem(FileSystemStorage::new);

        loadOverWorld(serverSettings);
        // loadNether(serverSettings);
        // loadEnd(serverSettings);

        MinecraftServer.getConnectionManager().addPlayerInitialization(player -> player.addEventCallback(PlayerLoginEvent.class, event -> {
            event.setSpawningInstance(worlds.get(0).getContainer());
            event.getPlayer().setRespawnPoint(worlds.get(0).getSpawn());
        }));

        MinecraftServer.getSchedulerManager().buildShutdownTask(() -> {
            try {
                worlds.forEach(world -> {
                    Leaf.getLogger().info("Saving world ??...");
                    world.getContainer().saveInstance(() -> Leaf.getLogger().info("World ?? has been saved!"));
                });
            } catch (Throwable e) {
                e.printStackTrace();
            }
        });
    }

    private void loadOverWorld(ServerSettings serverSettings) {
        InstanceContainer overworld = manager.createInstanceContainer(DimensionType.OVERWORLD, storageManager.getLocation(serverSettings.getOverWorldName() + "/data"));
        LeafWorld wrapper = new LeafWorld(serverSettings.getOverWorldName(), overworld);
        overworld.enableAutoChunkLoad(true);
        overworld.setChunkGenerator(new LeafChunkGenerator(wrapper));
        overworld.setExplosionSupplier(explosionSupplier);
        overworld.setData(new SerializableDataImpl());
        // overworld.setChunkLoader(new AnvilChunkLoader(storageManager.getLocation(serverSettings.getOverWorldName() + "/region")));
        worlds.add(wrapper);
        loadFewChunks(overworld);
    }

    private void loadNether(ServerSettings serverSettings) {
        if (!serverSettings.isNetherWorldEnabled()) return;
        InstanceContainer nether = manager.createInstanceContainer(LeafDimensionType.NETHER.getDimension(), storageManager.getLocation(serverSettings.getNetherName() + "/DIM-1/data"));
        LeafWorld wrapper = new LeafWorld(serverSettings.getNetherName(), nether);
        nether.enableAutoChunkLoad(true);
        nether.setExplosionSupplier(explosionSupplier);
        nether.setChunkGenerator(new LeafChunkGenerator(wrapper));
        nether.setData(new SerializableDataImpl());
        //   nether.setChunkLoader(new AnvilChunkLoader(storageManager.getLocation(serverSettings.getNetherName() + "/DIM-1/region")));
        worlds.add(wrapper);
        loadFewChunks(nether);
    }

    private void loadEnd(ServerSettings serverSettings) {
        if (!serverSettings.isEndWorldEnabled()) return;
        InstanceContainer end = manager.createInstanceContainer(LeafDimensionType.END.getDimension(), storageManager.getLocation(serverSettings.getEndWorldName() + "/DIM1/data"));
        end.enableAutoChunkLoad(true);
        end.setExplosionSupplier(explosionSupplier);
        end.setChunkGenerator(noiseTestGenerator);
        end.setData(new SerializableDataImpl());
        // end.setChunkLoader(new AnvilChunkLoader(storageManager.getLocation(serverSettings.getEndWorldName() + "/DIM1/region")));
        worlds.add(new LeafWorld(serverSettings.getEndWorldName(), end));
        loadFewChunks(end);
    }

    private void loadFewChunks(InstanceContainer container) {
        int loopStart = -2;
        int loopEnd = 2;
        for (int x = loopStart; x < loopEnd; x++)
            for (int z = loopStart; z < loopEnd; z++)
                container.loadChunk(x, z);
    }

    public Optional<LeafWorld> getByName(String name) {
        return worlds.stream().filter(leafWorld -> leafWorld.getName().equalsIgnoreCase(name)).findFirst();
    }
}
