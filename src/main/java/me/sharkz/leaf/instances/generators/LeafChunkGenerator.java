package me.sharkz.leaf.instances.generators;

import me.sharkz.leaf.instances.LeafWorld;
import me.sharkz.leaf.instances.generators.populators.TreePopulator;
import me.sharkz.leaf.utils.noise.PerlinNoiseGenerator;
import net.minestom.server.MinecraftServer;
import net.minestom.server.instance.Chunk;
import net.minestom.server.instance.ChunkGenerator;
import net.minestom.server.instance.ChunkPopulator;
import net.minestom.server.instance.batch.ChunkBatch;
import net.minestom.server.instance.block.Block;
import net.minestom.server.world.biomes.Biome;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class LeafChunkGenerator implements ChunkGenerator {


    private final Random random = new Random();
    private final PerlinNoiseGenerator generator;

    public LeafChunkGenerator(LeafWorld world) {
        this.generator = new PerlinNoiseGenerator(world.getSeed());
    }

    public int getHeight(int x, int z) {
        double preHeight = generator.getNoise(x / 16.0, z / 16.0, 8, 0.3d, 0.3d);
        return (int) ((preHeight > 0 ? preHeight * 6 : preHeight * 4) + 64);
    }

    @Override
    public void generateChunkData(@NotNull ChunkBatch batch, int chunkX, int chunkZ) {
        for (int x = 0; x < Chunk.CHUNK_SIZE_X; x++) {
            for (int z = 0; z < Chunk.CHUNK_SIZE_Z; z++) {
                final int height = getHeight(x + chunkX * 16, z + chunkZ * 16);
                for (int y = 0; y < height; y++) {
                    if (y == 0)
                        batch.setBlock(x, y, z, Block.BEDROCK);
                    else if (y == height - 1)
                        batch.setBlock(x, y, z, Block.GRASS_BLOCK);
                    else if (y > height - 7)
                        batch.setBlockStateId(x, y, z, Block.DIRT.getBlockId());
                    else
                        batch.setBlock(x, y, z, Block.STONE);
                }

                if (height < 61) {
                    batch.setBlock(x, height - 1, z, Block.DIRT);
                    for (int y = 0; y < 61 - height; y++)
                        batch.setBlock(x, y + height, z, Block.WATER);
                }
            }
        }
    }

    @Override
    public void fillBiomes(@NotNull Biome[] biomes, int chunkX, int chunkZ) {
        Arrays.fill(biomes, MinecraftServer.getBiomeManager().getById(0));
    }

    @Override
    public List<ChunkPopulator> getPopulators() {
        return Collections.singletonList(new TreePopulator());
    }
}
