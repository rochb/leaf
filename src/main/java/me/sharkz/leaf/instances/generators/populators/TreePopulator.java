package me.sharkz.leaf.instances.generators.populators;

import de.articdive.jnoise.JNoise;
import de.articdive.jnoise.interpolation.InterpolationType;
import me.sharkz.leaf.instances.generators.populators.trees.TreeStructure;
import net.minestom.server.instance.Chunk;
import net.minestom.server.instance.ChunkPopulator;
import net.minestom.server.instance.batch.ChunkBatch;
import net.minestom.server.utils.BlockPosition;

import java.util.Random;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class TreePopulator implements ChunkPopulator {

    private final JNoise noise;
    private final TreeStructure structure;

    public TreePopulator() {
        this.structure = new TreeStructure();
        this.noise = JNoise.newBuilder()
                .perlin()
                .setInterpolation(InterpolationType.LINEAR)
                .setSeed(new Random().nextInt())
                .setFrequency(0.6)
                .build();
    }

    @Override
    public void populateChunk(ChunkBatch batch, Chunk chunk) {
        for (int x = -2; x < 18; x++) {
            for (int z = -2; z < 18; z++) {
                if (noise.getNoise(x + chunk.getChunkX() * 16, z + chunk.getChunkZ() * 16) > 0.75) {
                    int y = getHeight(x + chunk.getChunkX() * 16, z + chunk.getChunkZ() * 16);
                    if (chunk.getBlockStateId(x, y - 1, z) == 0) continue;
                    structure.build(batch, new BlockPosition(x, y, y));
                }
            }
        }
    }

    private int getHeight(int x, int z) {
        double preHeight = noise.getNoise(x / 16.0, z / 16.0);
        return (int) ((preHeight > 0 ? preHeight * 6 : preHeight * 4) + 64);
    }
}