package me.sharkz.leaf.instances.generators.populators.trees;

import me.sharkz.leaf.instances.generators.Structure;
import net.minestom.server.instance.block.Block;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class TreeStructure extends Structure {

    public TreeStructure() {
        this.addBlock(Block.DIRT, 0, -1, 0);
        this.addBlock(Block.OAK_LOG, 0, 0, 0);
        this.addBlock(Block.OAK_LOG, 0, 1, 0);
        this.addBlock(Block.OAK_LOG, 0, 2, 0);
        this.addBlock(Block.OAK_LOG, 0, 3, 0);

        this.addBlock(Block.OAK_LEAVES, 1, 1, 0);
        this.addBlock(Block.OAK_LEAVES, 2, 1, 0);
        this.addBlock(Block.OAK_LEAVES, -1, 1, 0);
        this.addBlock(Block.OAK_LEAVES, -2, 1, 0);

        this.addBlock(Block.OAK_LEAVES, 1, 1, 1);
        this.addBlock(Block.OAK_LEAVES, 2, 1, 1);
        this.addBlock(Block.OAK_LEAVES, 0, 1, 1);
        this.addBlock(Block.OAK_LEAVES, -1, 1, 1);
        this.addBlock(Block.OAK_LEAVES, -2, 1, 1);

        this.addBlock(Block.OAK_LEAVES, 1, 1, 2);
        this.addBlock(Block.OAK_LEAVES, 2, 1, 2);
        this.addBlock(Block.OAK_LEAVES, 0, 1, 2);
        this.addBlock(Block.OAK_LEAVES, -1, 1, 2);
        this.addBlock(Block.OAK_LEAVES, -2, 1, 2);

        this.addBlock(Block.OAK_LEAVES, 1, 1, -1);
        this.addBlock(Block.OAK_LEAVES, 2, 1, -1);
        this.addBlock(Block.OAK_LEAVES, 0, 1, -1);
        this.addBlock(Block.OAK_LEAVES, -1, 1, -1);
        this.addBlock(Block.OAK_LEAVES, -2, 1, -1);

        this.addBlock(Block.OAK_LEAVES, 1, 1, -2);
        this.addBlock(Block.OAK_LEAVES, 2, 1, -2);
        this.addBlock(Block.OAK_LEAVES, 0, 1, -2);
        this.addBlock(Block.OAK_LEAVES, -1, 1, -2);
        this.addBlock(Block.OAK_LEAVES, -2, 1, -2);

        this.addBlock(Block.OAK_LEAVES, 1, 2, 0);
        this.addBlock(Block.OAK_LEAVES, 2, 2, 0);
        this.addBlock(Block.OAK_LEAVES, -1, 2, 0);
        this.addBlock(Block.OAK_LEAVES, -2, 2, 0);

        this.addBlock(Block.OAK_LEAVES, 1, 2, 1);
        this.addBlock(Block.OAK_LEAVES, 2, 2, 1);
        this.addBlock(Block.OAK_LEAVES, 0, 2, 1);
        this.addBlock(Block.OAK_LEAVES, -1, 2, 1);
        this.addBlock(Block.OAK_LEAVES, -2, 2, 1);

        this.addBlock(Block.OAK_LEAVES, 1, 2, 2);
        this.addBlock(Block.OAK_LEAVES, 2, 2, 2);
        this.addBlock(Block.OAK_LEAVES, 0, 2, 2);
        this.addBlock(Block.OAK_LEAVES, -1, 2, 2);
        this.addBlock(Block.OAK_LEAVES, -2, 2, 2);

        this.addBlock(Block.OAK_LEAVES, 1, 2, -1);
        this.addBlock(Block.OAK_LEAVES, 2, 2, -1);
        this.addBlock(Block.OAK_LEAVES, 0, 2, -1);
        this.addBlock(Block.OAK_LEAVES, -1, 2, -1);
        this.addBlock(Block.OAK_LEAVES, -2, 2, -1);

        this.addBlock(Block.OAK_LEAVES, 1, 2, -2);
        this.addBlock(Block.OAK_LEAVES, 2, 2, -2);
        this.addBlock(Block.OAK_LEAVES, 0, 2, -2);
        this.addBlock(Block.OAK_LEAVES, -1, 2, -2);
        this.addBlock(Block.OAK_LEAVES, -2, 2, -2);

        this.addBlock(Block.OAK_LEAVES, 1, 3, 0);
        this.addBlock(Block.OAK_LEAVES, -1, 3, 0);

        this.addBlock(Block.OAK_LEAVES, 1, 3, 1);
        this.addBlock(Block.OAK_LEAVES, 0, 3, 1);
        this.addBlock(Block.OAK_LEAVES, -1, 3, 1);

        this.addBlock(Block.OAK_LEAVES, 1, 3, -1);
        this.addBlock(Block.OAK_LEAVES, 0, 3, -1);
        this.addBlock(Block.OAK_LEAVES, -1, 3, -1);

        this.addBlock(Block.OAK_LEAVES, 1, 4, 0);
        this.addBlock(Block.OAK_LEAVES, 0, 4, 0);
        this.addBlock(Block.OAK_LEAVES, -1, 4, 0);

        this.addBlock(Block.OAK_LEAVES, 0, 4, 1);

        this.addBlock(Block.OAK_LEAVES, 0, 4, -1);
        this.addBlock(Block.OAK_LEAVES, -1, 4, -1);
    }
}
