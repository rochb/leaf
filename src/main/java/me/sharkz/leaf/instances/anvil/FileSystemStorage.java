package me.sharkz.leaf.instances.anvil;

import net.minestom.server.storage.StorageOptions;
import net.minestom.server.storage.StorageSystem;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class FileSystemStorage implements StorageSystem {

    @Override
    public void open(@NotNull String folderPath, @NotNull StorageOptions options) {
    }

    @Override
    public void close() {
    }

    @Override
    public boolean exists(@NotNull String folderPath) {
        return Files.exists(Path.of(folderPath));
    }

    @Override
    public byte[] get(@NotNull String key) {
        try {
            Path path = getPath(key);
            if (!Files.exists(path)) {
                return null;
            }
            return Files.readAllBytes(path);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Path getPath(String key) {
        return Path.of(key + ".dat");
    }

    @Override
    public void set(@NotNull String key, byte[] data) {
        try {
            Files.write(getPath(key), data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(@NotNull String key) {
        try {
            Files.delete(getPath(key));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}