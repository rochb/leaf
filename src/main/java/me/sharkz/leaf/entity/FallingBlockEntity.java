package me.sharkz.leaf.entity;

import me.sharkz.leaf.Leaf;
import net.minestom.server.entity.EntityType;
import net.minestom.server.entity.ItemEntity;
import net.minestom.server.entity.Metadata;
import net.minestom.server.entity.ObjectEntity;
import net.minestom.server.entity.metadata.other.FallingBlockMeta;
import net.minestom.server.instance.block.Block;
import net.minestom.server.instance.block.CustomBlock;
import net.minestom.server.item.ItemStack;
import net.minestom.server.item.Material;
import net.minestom.server.network.packet.server.play.EntityMetaDataPacket;
import net.minestom.server.utils.BlockPosition;
import net.minestom.server.utils.Position;
import org.jetbrains.annotations.NotNull;

import java.util.Random;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class FallingBlockEntity extends ObjectEntity {

    private final Block baseBlock;
    private final CustomBlock toPlace;

    public FallingBlockEntity(Block baseBlock, CustomBlock toPlace, Position initialPosition) {
        super(EntityType.FALLING_BLOCK, initialPosition);
        this.baseBlock = baseBlock;
        this.toPlace = toPlace;
        setGravity(0.025f, getGravityAcceleration(), getGravityTerminalVelocity());
        setBoundingBox(0.98f, 0.98f, 0.98f);
    }

    @Override
    public int getObjectData() {
        return baseBlock.getBlockId();
    }

    @Override
    public void spawn() {

    }

    @Override
    public void update(long time) {
        if (!isOnGround()) return;
        BlockPosition position = getPosition().toBlockPosition().subtract(0, 1, 0);
        if (instance.getBlockStateId(position) != Block.AIR.getBlockId()) {
            Leaf.getLogger().info("Material: " + baseBlock.name() + " - " + Material.valueOf(baseBlock.name()).name());
            ItemStack stack = ItemStack.of(Material.SAND, (byte) 1);
            ItemEntity itemForm = new ItemEntity(stack, new Position(position.getX() + 0.5f, position.getY(), position.getZ() + 0.5f));

            Random rng = new Random();
            itemForm.getVelocity().setX((float) rng.nextGaussian() * 2f);
            itemForm.getVelocity().setY(rng.nextFloat() * 2.5f + 2.5f);
            itemForm.getVelocity().setZ((float) rng.nextGaussian() * 2f);

            itemForm.setInstance(instance);
        } else {
            if (toPlace != null)
                instance.setSeparateBlocks(position.getX(), position.getY(), position.getZ(), baseBlock.getBlockId(), toPlace.getCustomBlockId());
            else
                instance.setBlock(getPosition().toBlockPosition(), baseBlock);
        }
        remove();
    }

}