package me.sharkz.leaf.commands;

import lombok.NonNull;
import net.minestom.server.MinecraftServer;
import net.minestom.server.command.CommandSender;
import net.minestom.server.command.builder.Command;
import net.minestom.server.command.builder.CommandContext;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ShutdownCommand extends Command {

    public ShutdownCommand() {
        super("shutdown", "stop");
        addSyntax(this::execute);
    }

    private void execute(@NonNull CommandSender commandSender, @NonNull CommandContext commandContext) {
        MinecraftServer.stopCleanly();
    }
}