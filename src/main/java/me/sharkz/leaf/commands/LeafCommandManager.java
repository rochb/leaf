package me.sharkz.leaf.commands;

import lombok.Getter;
import me.sharkz.leaf.Leaf;
import me.sharkz.leaf.utils.ObjectInjector;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.minestom.server.MinecraftServer;
import net.minestom.server.command.CommandManager;
import net.minestom.server.command.builder.Command;

import java.util.List;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class LeafCommandManager extends ObjectInjector<Command> {

    private final CommandManager manager;
    @Getter
    private final List<Command> commands;

    public LeafCommandManager() {
        this.manager = MinecraftServer.getCommandManager();
        this.manager.setUnknownCommandCallback((sender, command) -> sender.sendMessage(Component.text("Unknown command", NamedTextColor.RED)));

        Leaf.getLogger().info("Trying to inject commands...");
        long start = System.currentTimeMillis();
        commands = inject("me.sharkz.leaf.commands", Command.class);
        commands.forEach(manager::register);
        Leaf.getLogger().info(commands.size() + " commands were been injected! (Took " + (System.currentTimeMillis() - start) + " ms)");
    }

}
