package me.sharkz.leaf.commands;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.minestom.server.MinecraftServer;
import net.minestom.server.command.CommandSender;
import net.minestom.server.command.builder.Arguments;
import net.minestom.server.command.builder.Command;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class SaveAllCommand extends Command {

    public SaveAllCommand() {
        super("save-all");
        setCondition(this::condition);
        setDefaultExecutor(this::execute);
    }

    private boolean condition(CommandSender player, String commandName) {
        return true; // TODO: permissions
    }

    private void execute(CommandSender player, Arguments arguments) {
        MinecraftServer.getInstanceManager().getInstances().forEach(i -> i.saveChunksToStorage(() -> System.out.println("Saved dimension " + i.getDimensionType().getName())));
        player.sendMessage(Component.text("All instances were been saved!", NamedTextColor.GREEN));
    }
}