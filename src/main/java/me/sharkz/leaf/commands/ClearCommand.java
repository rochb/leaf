package me.sharkz.leaf.commands;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.event.ClickEvent;
import net.minestom.server.command.CommandSender;
import net.minestom.server.command.builder.Command;
import net.minestom.server.command.builder.CommandContext;
import net.minestom.server.command.builder.arguments.ArgumentType;
import net.minestom.server.command.builder.condition.Conditions;
import net.minestom.server.command.builder.exception.ArgumentSyntaxException;
import net.minestom.server.entity.Player;
import net.minestom.server.utils.entity.EntityFinder;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ClearCommand extends Command {

    public ClearCommand() {
        super("clear", "clean");

        setCondition(Conditions::playerOnly);

        setDefaultExecutor(this::usage);

        var player = ArgumentType.Entity("player")
                .onlyPlayers(true)
                .singleEntity(true);

        setArgumentCallback(this::targetCallback, player);

        addSyntax(this::executeOnSelf);
        addSyntax(this::executeOnOther, player);
    }

    private void usage(CommandSender sender, CommandContext context) {
        sender.sendMessage(Component.text("Usage: /clear [player]")
                .hoverEvent(Component.text("Click to get this command."))
                .clickEvent(ClickEvent.suggestCommand("/clear player")));
    }

    private void executeOnSelf(CommandSender sender, CommandContext context) {
        Player player = (Player) sender;
        player.getInventory().clear();
        player.sendMessage(Component.text("Your inventory has been cleared!"));
    }

    private void executeOnOther(CommandSender sender, CommandContext context) {
        EntityFinder targetFinder = context.get("player");
        Player target = targetFinder.findFirstPlayer(sender);
        assert target != null;
        target.getInventory().clear();
        target.sendMessage(Component.text(target.getUsername() + "'s inventory has been cleared!"));
    }

    private void targetCallback(CommandSender sender, ArgumentSyntaxException exception) {
        sender.sendMessage(Component.text("'" + exception.getInput() + "' is not a valid player name."));
    }

}
