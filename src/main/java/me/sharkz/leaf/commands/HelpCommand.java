package me.sharkz.leaf.commands;

import me.sharkz.leaf.Leaf;
import net.minestom.server.command.CommandSender;
import net.minestom.server.command.builder.Arguments;
import net.minestom.server.command.builder.Command;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class HelpCommand extends Command {
    public HelpCommand() {
        super("help");

        setDefaultExecutor(this::execute);
    }

    private void execute(CommandSender player, Arguments arguments) {
        player.sendMessage("=== Help ===");
        Leaf.get()
                .getCommandManager()
                .getCommands()
                .stream()
                .sorted()
                .forEach(command -> player.sendMessage("/" + command.getName().toLowerCase()));
        player.sendMessage("============");
    }

    private int compareCommands(Command a, Command b) {
        return a.getName().compareTo(b.getName());
    }
}