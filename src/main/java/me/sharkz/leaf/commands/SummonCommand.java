package me.sharkz.leaf.commands;

import lombok.NonNull;
import net.minestom.server.command.CommandSender;
import net.minestom.server.command.builder.Command;
import net.minestom.server.command.builder.CommandContext;
import net.minestom.server.command.builder.arguments.ArgumentType;
import net.minestom.server.command.builder.arguments.minecraft.registry.ArgumentEntityType;
import net.minestom.server.command.builder.arguments.relative.ArgumentRelativeVec3;
import net.minestom.server.command.builder.condition.Conditions;
import net.minestom.server.entity.Entity;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class SummonCommand extends Command {

    private final ArgumentEntityType entity;
    private final ArgumentRelativeVec3 pos;

    public SummonCommand() {
        super("summon");
        setCondition(Conditions::playerOnly);

        entity = ArgumentType.EntityType("entity type");
        pos = ArgumentType.RelativeVec3("pos");
        addSyntax(this::execute, entity, pos);
    }

    private void execute(@NonNull CommandSender commandSender, @NonNull CommandContext commandContext) {
        final Entity entity = new Entity(commandContext.get(this.entity));
        entity.setInstance(commandSender.asPlayer().getInstance(), commandContext.get(pos).from(commandSender.asPlayer()).toPosition());
    }
}