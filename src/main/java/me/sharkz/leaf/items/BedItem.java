package me.sharkz.leaf.items;

import me.sharkz.leaf.blocks.BedBlock;
import me.sharkz.leaf.registry.LeafBlockRegistryManager;
import me.sharkz.leaf.utils.InventoryUtils;
import net.minestom.server.entity.Player;
import net.minestom.server.instance.Instance;
import net.minestom.server.instance.block.Block;
import net.minestom.server.item.ItemStack;
import net.minestom.server.item.Material;
import net.minestom.server.utils.BlockPosition;
import net.minestom.server.utils.Direction;
import net.minestom.server.utils.MathUtils;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class BedItem extends LeafItem {

    public BedItem(Material baseItem) {
        super(baseItem);
    }

    @Override
    public boolean onUseOnBlock(Player player, ItemStack itemStack, Player.Hand hand, BlockPosition position, Direction blockFace) {
        BlockPosition abovePos = new BlockPosition(position.getX(), position.getY() + 1, position.getZ());
        Instance instance = player.getInstance();
        Block above = Block.fromStateId(instance.getBlockStateId(abovePos));
        if (isReplaceable(above)) {
            Direction playerDirection = MathUtils.getHorizontalDirection(player.getPosition().getYaw());
            BlockPosition bedHeadPosition = new BlockPosition(abovePos.getX(), abovePos.getY(), abovePos.getZ());
            bedHeadPosition.add(playerDirection.normalX(), playerDirection.normalY(), playerDirection.normalZ());
            Block blockAtPotentialBedHead = Block.fromStateId(instance.getBlockStateId(bedHeadPosition));
            if (isReplaceable(blockAtPotentialBedHead)) {
                placeBed(instance, abovePos, bedHeadPosition, playerDirection);
                InventoryUtils.consumeItemIfNotCreative(player, itemStack, hand);
            }
        }
        return true;
    }


    private void placeBed(Instance instance, BlockPosition footPosition, BlockPosition headPosition, Direction facing) {
        LeafBlockRegistryManager.fromMaterial(getMaterial()).ifPresent(bedBlock -> {
            short footId = ((BedBlock) bedBlock).getBaseBlockState().with("facing", facing.name().toLowerCase()).with("part", "foot").getBlockId();
            short headId = ((BedBlock) bedBlock).getBaseBlockState().with("facing", facing.name().toLowerCase()).with("part", "head").getBlockId();
            instance.setSeparateBlocks(footPosition.getX(), footPosition.getY(), footPosition.getZ(), footId, bedBlock.getCustomBlockId(), null);
            instance.setSeparateBlocks(headPosition.getX(), headPosition.getY(), headPosition.getZ(), headId, bedBlock.getCustomBlockId(), null);
        });
    }

    private boolean isReplaceable(Block blockAtPosition) {
        return blockAtPosition.isAir() || blockAtPosition.isLiquid();
    }

    @Override
    public void onUseInAir(Player player, ItemStack itemStack, Player.Hand hand) {
    }
}