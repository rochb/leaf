package me.sharkz.leaf.items;

import net.minestom.server.MinecraftServer;
import net.minestom.server.entity.Player;
import net.minestom.server.instance.Instance;
import net.minestom.server.instance.block.Block;
import net.minestom.server.instance.block.BlockFace;
import net.minestom.server.instance.block.CustomBlock;
import net.minestom.server.item.ItemStack;
import net.minestom.server.item.Material;
import net.minestom.server.utils.BlockPosition;
import net.minestom.server.utils.Direction;

import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class JukeboxItem extends LeafItem {

    public JukeboxItem() {
        super(Material.JUKEBOX);
    }

    @Override
    public void onUseInAir(Player player, ItemStack itemStack, Player.Hand hand) {

    }

    @Override
    public boolean onUseOnBlock(Player player, ItemStack itemStack, Player.Hand hand, BlockPosition position, Direction blockFace) {
        BlockPosition blockPosition = new BlockPosition(position.getX(), position.getY(), position.getZ());

        Instance instance = player.getInstance();
        if (Objects.isNull(instance) || !instance.getBlock(blockPosition).isAir()) return false;

        CustomBlock block = MinecraftServer.getBlockManager().getCustomBlock(Block.JUKEBOX.getBlockId());
        if (!player.isCreative())
            LeafItem.damageItem(player, hand, itemStack);
        if (block != null)
            instance.setCustomBlock(blockPosition.getX(), blockPosition.getY(), blockPosition.getZ(), block.getCustomBlockId());
        else
            instance.setBlock(blockPosition, Block.JUKEBOX);
        return false;
    }
}
