package me.sharkz.leaf.items;

import me.sharkz.leaf.Leaf;
import net.minestom.server.MinecraftServer;
import net.minestom.server.entity.Player;
import net.minestom.server.instance.Instance;
import net.minestom.server.instance.block.Block;
import net.minestom.server.instance.block.BlockFace;
import net.minestom.server.instance.block.CustomBlock;
import net.minestom.server.item.ItemStack;
import net.minestom.server.item.Material;
import net.minestom.server.utils.BlockPosition;
import net.minestom.server.utils.Direction;

import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class FlintAndSteel extends LeafItem {

    public FlintAndSteel() {
        super(Material.FLINT_AND_STEEL);
    }

    @Override
    public void onUseInAir(Player player, ItemStack itemStack, Player.Hand hand) {

    }

    @Override
    public boolean onUseOnBlock(Player player, ItemStack itemStack, Player.Hand hand, BlockPosition position, Direction blockFace) {
        BlockPosition firePosition = new BlockPosition(position.getX(), position.getY(), position.getZ()).getRelative(BlockFace.TOP);

        Instance instance = player.getInstance();
        if (Objects.isNull(instance) || !instance.getBlock(firePosition).isAir()) return false;

        CustomBlock fireBlock = MinecraftServer.getBlockManager().getCustomBlock(Block.FIRE.getBlockId());
        if (!player.isCreative())
            LeafItem.damageItem(player, hand, itemStack);
        if (fireBlock != null)
            instance.setCustomBlock(firePosition.getX(), firePosition.getY(), firePosition.getZ(), fireBlock.getCustomBlockId());
        else
            instance.setBlock(firePosition, Block.FIRE);
        return false;
    }

}