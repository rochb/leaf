package me.sharkz.leaf.items;

import me.sharkz.leaf.registry.LeafBlockRegistryManager;
import net.minestom.server.entity.Player;
import net.minestom.server.instance.Instance;
import net.minestom.server.item.ItemStack;
import net.minestom.server.item.Material;
import net.minestom.server.utils.BlockPosition;
import net.minestom.server.utils.Direction;

import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class GravityItem extends LeafItem {

    public GravityItem(Material material) {
        super(material);
    }

    @Override
    public void onUseInAir(Player player, ItemStack itemStack, Player.Hand hand) {

    }

    @Override
    public boolean onUseOnBlock(Player player, ItemStack itemStack, Player.Hand hand, BlockPosition position, Direction blockFace) {
        BlockPosition above = position.clone().add(blockFace.normalX(), blockFace.normalY(), blockFace.normalZ());

        Instance instance = player.getInstance();
        if (Objects.isNull(instance) || !instance.getBlock(above).isAir()) return false;
        LeafBlockRegistryManager.fromMaterial(getMaterial())
                .ifPresent(gravityBlock -> {
                    if (!player.isCreative())
                        LeafItem.damageItem(player, hand, itemStack);
                    instance.setCustomBlock(above.getX(), above.getY(), above.getZ(), gravityBlock.getCustomBlockId());
                });
        return false;
    }
}
