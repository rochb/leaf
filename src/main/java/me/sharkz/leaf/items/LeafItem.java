package me.sharkz.leaf.items;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minestom.server.entity.Player;
import net.minestom.server.item.ItemMeta;
import net.minestom.server.item.ItemStack;
import net.minestom.server.item.Material;
import net.minestom.server.utils.BlockPosition;
import net.minestom.server.utils.Direction;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
@RequiredArgsConstructor
public abstract class LeafItem {

    private final Material material;

    /**
     * Called when the player right clicks with this item in the air
     *
     * @param player
     * @param itemStack
     * @param hand
     */
    public abstract void onUseInAir(Player player, ItemStack itemStack, Player.Hand hand);

    /**
     * Called when the player right clicks with this item on a block
     *
     * @param player
     * @param itemStack
     * @param hand
     * @param position
     * @param blockFace
     * @return true if it prevents normal item use (placing blocks for instance)
     */
    public abstract boolean onUseOnBlock(Player player, ItemStack itemStack, Player.Hand hand, BlockPosition position, Direction blockFace);

    public static void damageItem(Player player, Player.Hand hand, ItemStack itemStack) {
        ItemMeta meta = itemStack.getMeta();
        ItemStack newItemStack = itemStack.withMeta(itemMetaBuilder -> itemMetaBuilder.damage(meta.getDamage() + 1));

        if (hand == Player.Hand.OFF)
            player.getInventory().setItemInOffHand(newItemStack);
        else
            player.getInventory().setItemInMainHand(newItemStack);
    }
}