package me.sharkz.leaf.items;

import me.sharkz.leaf.Leaf;
import me.sharkz.leaf.utils.ObjectInjector;
import net.minestom.server.MinecraftServer;
import net.minestom.server.data.Data;
import net.minestom.server.event.player.PlayerBlockInteractEvent;
import net.minestom.server.event.player.PlayerUseItemEvent;
import net.minestom.server.event.player.PlayerUseItemOnBlockEvent;
import net.minestom.server.instance.Instance;
import net.minestom.server.instance.block.CustomBlock;
import net.minestom.server.item.ItemStack;
import net.minestom.server.network.ConnectionManager;
import net.minestom.server.utils.BlockPosition;

import java.util.List;
import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class LeafItemManager extends ObjectInjector<LeafItem> {

    private final ConnectionManager manager;

    public LeafItemManager() {
        this.manager = MinecraftServer.getConnectionManager();

        Leaf.getLogger().info("Trying to inject commands...");
        long start = System.currentTimeMillis();

        /* Injection */
        List<LeafItem> items = inject("me.sharkz.leaf.items", LeafItem.class);
        items.forEach(this::register);

        Leaf.getLogger().info(items.size() + " leaf items were been injected! (Took " + (System.currentTimeMillis() - start) + " ms)");
    }

    public void register(LeafItem item) {
        manager.addPlayerInitialization(player -> {
            player.addEventCallback(PlayerUseItemEvent.class, event -> {
                if (event.getItemStack().getMaterial() == item.getMaterial())
                    item.onUseInAir(player, event.getItemStack(), event.getHand());
            });

            player.addEventCallback(PlayerBlockInteractEvent.class, event -> {
                Instance instance = player.getInstance();
                BlockPosition blockPosition = event.getBlockPosition();

                CustomBlock customBlock = Objects.requireNonNull(instance).getCustomBlock(blockPosition);
                if (customBlock != null) {
                    Data data = instance.getBlockData(blockPosition);
                    boolean blocksItem = customBlock.onInteract(player, event.getHand(), blockPosition, data);
                    if (blocksItem) {
                        event.setBlockingItemUse(true);
                        event.setCancelled(true);
                    }
                }

                if (!event.isCancelled()) {
                    ItemStack itemStack = player.getItemInHand(event.getHand());
                    if (itemStack.getMaterial() == item.getMaterial()) {
                        if (item.onUseOnBlock(player, itemStack, event.getHand(), event.getBlockPosition(), event.getBlockFace().toDirection())) {
                            event.setBlockingItemUse(true);
                            event.setCancelled(true);
                        }
                    }
                }
            });

            player.addEventCallback(PlayerUseItemOnBlockEvent.class, event -> {
                if (event.getItemStack().getMaterial() == item.getMaterial())
                    item.onUseOnBlock(player, event.getItemStack(), event.getHand(), event.getPosition(), event.getBlockFace());
            });
        });
    }


}
