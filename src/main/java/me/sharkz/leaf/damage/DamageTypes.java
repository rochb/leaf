package me.sharkz.leaf.damage;

import lombok.Getter;
import net.minestom.server.entity.damage.DamageType;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public enum DamageTypes {
    IN_FIRE("attack.inFire"),
    EXPLOSION("attack.explosion");
    private final DamageType type;

    DamageTypes(String identifier) {
        this.type = new DamageType(identifier);
    }
}
