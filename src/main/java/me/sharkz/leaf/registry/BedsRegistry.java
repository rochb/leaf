package me.sharkz.leaf.registry;

import me.sharkz.leaf.blocks.BedBlock;
import me.sharkz.leaf.blocks.LeafBlockManager;
import me.sharkz.leaf.items.BedItem;
import me.sharkz.leaf.items.LeafItemManager;
import net.minestom.server.instance.block.Block;
import net.minestom.server.item.Material;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class BedsRegistry implements BlocksRegistry<BedBlock> {

    private final Map<Material, BedBlock> blocks = new HashMap<>();

    public BedsRegistry() {
        blocks.put(Material.WHITE_BED, new BedBlock(Block.WHITE_BED));
        blocks.put(Material.BLACK_BED, new BedBlock(Block.BLACK_BED));
        blocks.put(Material.LIGHT_BLUE_BED, new BedBlock(Block.LIGHT_BLUE_BED));
        blocks.put(Material.BLUE_BED, new BedBlock(Block.BLUE_BED));
        blocks.put(Material.RED_BED, new BedBlock(Block.RED_BED));
        blocks.put(Material.GREEN_BED, new BedBlock(Block.GREEN_BED));
        blocks.put(Material.YELLOW_BED, new BedBlock(Block.YELLOW_BED));
        blocks.put(Material.PURPLE_BED, new BedBlock(Block.PURPLE_BED));
        blocks.put(Material.MAGENTA_BED, new BedBlock(Block.MAGENTA_BED));
        blocks.put(Material.CYAN_BED, new BedBlock(Block.CYAN_BED));
        blocks.put(Material.PINK_BED, new BedBlock(Block.PINK_BED));
        blocks.put(Material.GRAY_BED, new BedBlock(Block.GRAY_BED));
        blocks.put(Material.LIGHT_GRAY_BED, new BedBlock(Block.LIGHT_GRAY_BED));
        blocks.put(Material.ORANGE_BED, new BedBlock(Block.ORANGE_BED));
        blocks.put(Material.BROWN_BED, new BedBlock(Block.BROWN_BED));
        blocks.put(Material.LIME_BED, new BedBlock(Block.LIME_BED));
    }

    @Override
    public void registerBlocks(LeafBlockManager manager) {
        blocks.values().forEach(manager::register);
    }

    @Override
    public void registerItem(LeafItemManager manager) {
        blocks.keySet().forEach(material -> manager.register(new BedItem(material)));
    }

    @Override
    public Optional<BedBlock> fromMaterial(Material material) {
        return Optional.ofNullable(blocks.get(material));
    }
}
