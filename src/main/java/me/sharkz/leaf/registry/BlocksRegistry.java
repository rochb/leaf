package me.sharkz.leaf.registry;

import me.sharkz.leaf.blocks.LeafBlockManager;
import me.sharkz.leaf.items.LeafItemManager;
import net.minestom.server.instance.block.CustomBlock;
import net.minestom.server.item.Material;

import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface BlocksRegistry<T extends CustomBlock> {

    void registerBlocks(LeafBlockManager manager);

    void registerItem(LeafItemManager manager);

    Optional<T> fromMaterial(Material material);
}
