package me.sharkz.leaf.registry;

import me.sharkz.leaf.Leaf;
import me.sharkz.leaf.blocks.LeafBlockManager;
import me.sharkz.leaf.items.LeafItemManager;
import me.sharkz.leaf.utils.ObjectInjector;
import net.minestom.server.instance.block.CustomBlock;
import net.minestom.server.item.Material;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class LeafBlockRegistryManager extends ObjectInjector<BlocksRegistry<?>> {

    private List<BlocksRegistry<?>> registries;
    private static LeafBlockRegistryManager instance;

    public LeafBlockRegistryManager(LeafItemManager itemManager, LeafBlockManager blockManager) {
        instance = this;
        Leaf.getLogger().info("Trying to inject leaf registries...");
        long start = System.currentTimeMillis();
        registries = inject("me.sharkz.leaf.registry", BlocksRegistry.class);
        registries.forEach(blocksRegistry -> {
            blocksRegistry.registerBlocks(blockManager);
            blocksRegistry.registerItem(itemManager);
        });
        Leaf.getLogger().info(registries.size() + " leaf registries were been injected! (Took " + (System.currentTimeMillis() - start) + " ms)");
    }

    public static Optional<CustomBlock> fromMaterial(Material material) {
        if (Objects.isNull(instance)) return Optional.empty();
        return (Optional<CustomBlock>) instance.registries.stream()
                .map(blocksRegistry -> blocksRegistry.fromMaterial(material))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .findFirst();
    }

}
