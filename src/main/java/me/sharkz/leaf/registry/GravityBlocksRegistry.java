package me.sharkz.leaf.registry;

import com.google.common.collect.ImmutableMap;
import me.sharkz.leaf.blocks.GravityBlock;
import me.sharkz.leaf.blocks.LeafBlockManager;
import me.sharkz.leaf.items.GravityItem;
import me.sharkz.leaf.items.LeafItemManager;
import net.minestom.server.instance.block.Block;
import net.minestom.server.item.Material;

import java.util.Map;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class GravityBlocksRegistry implements BlocksRegistry<GravityBlock> {

    private final Map<Material, GravityBlock> blocks = ImmutableMap.of(Material.SAND, new GravityBlock(Block.SAND),
            Material.RED_SAND, new GravityBlock(Block.RED_SAND),
            Material.GRAVEL, new GravityBlock(Block.GRAVEL));

    @Override
    public void registerBlocks(LeafBlockManager manager) {
        blocks.values().forEach(manager::register);
    }

    @Override
    public void registerItem(LeafItemManager manager) {
        blocks.keySet().forEach(material -> manager.register(new GravityItem(material)));
    }

    @Override
    public Optional<GravityBlock> fromMaterial(Material material) {
        return Optional.ofNullable(blocks.get(material));
    }
}
