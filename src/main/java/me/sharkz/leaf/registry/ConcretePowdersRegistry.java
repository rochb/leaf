package me.sharkz.leaf.registry;

import me.sharkz.leaf.blocks.ConcretePowderBlock;
import me.sharkz.leaf.blocks.LeafBlockManager;
import me.sharkz.leaf.items.LeafItemManager;
import net.minestom.server.instance.block.Block;
import net.minestom.server.item.Material;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ConcretePowdersRegistry implements BlocksRegistry<ConcretePowderBlock> {

    private final Map<Block, Block> blocks = new HashMap<>();

    public ConcretePowdersRegistry() {
        blocks.put(Block.WHITE_CONCRETE_POWDER, Block.WHITE_CONCRETE);
        blocks.put(Block.BLACK_CONCRETE_POWDER, Block.BLACK_CONCRETE);
        blocks.put(Block.LIGHT_BLUE_CONCRETE_POWDER, Block.LIGHT_BLUE_CONCRETE);
        blocks.put(Block.BLUE_CONCRETE_POWDER, Block.BLUE_CONCRETE);
        blocks.put(Block.RED_CONCRETE_POWDER, Block.RED_CONCRETE);
        blocks.put(Block.GREEN_CONCRETE_POWDER, Block.GREEN_CONCRETE);
        blocks.put(Block.YELLOW_CONCRETE_POWDER, Block.YELLOW_CONCRETE);
        blocks.put(Block.PURPLE_CONCRETE_POWDER, Block.PURPLE_CONCRETE);
        blocks.put(Block.MAGENTA_CONCRETE_POWDER, Block.MAGENTA_CONCRETE);
        blocks.put(Block.CYAN_CONCRETE_POWDER, Block.CYAN_CONCRETE);
        blocks.put(Block.PINK_CONCRETE_POWDER, Block.PINK_CONCRETE);
        blocks.put(Block.GRAY_CONCRETE_POWDER, Block.GRAY_CONCRETE);
        blocks.put(Block.LIGHT_GRAY_CONCRETE_POWDER, Block.LIGHT_GRAY_CONCRETE);
        blocks.put(Block.ORANGE_CONCRETE_POWDER, Block.ORANGE_CONCRETE);
        blocks.put(Block.BROWN_CONCRETE_POWDER, Block.BROWN_CONCRETE);
        blocks.put(Block.LIME_CONCRETE_POWDER, Block.LIME_CONCRETE);
    }

    @Override
    public void registerBlocks(LeafBlockManager manager) {
        blocks.forEach((block, block2) -> manager.register(new ConcretePowderBlock(block, block2)));
    }

    @Override
    public void registerItem(LeafItemManager manager) {

    }

    @Override
    public Optional<ConcretePowderBlock> fromMaterial(Material material) {
        return Optional.empty();
    }
}
