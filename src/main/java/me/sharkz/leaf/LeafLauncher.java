package me.sharkz.leaf;


/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class LeafLauncher {

    /**
     * Start method
     *
     * @param args cli arguments
     */
    public static void main(String[] args) {
        new Leaf();
    }

}
