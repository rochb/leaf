package me.sharkz.leaf.events.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minestom.server.entity.Entity;
import net.minestom.server.event.Event;
import net.minestom.server.instance.block.states.NetherPortal;
import net.minestom.server.utils.BlockPosition;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@RequiredArgsConstructor
@Getter
public class NetherPortalUpdateEvent extends Event {

    private final Entity entity;
    private final BlockPosition position;
    private final NetherPortal portal;
    private final long tickSpentInPortal;

}