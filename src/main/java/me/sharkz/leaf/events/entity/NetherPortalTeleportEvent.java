package me.sharkz.leaf.events.entity;

import lombok.Getter;
import lombok.Setter;
import net.minestom.server.entity.Entity;
import net.minestom.server.event.CancellableEvent;
import net.minestom.server.event.Event;
import net.minestom.server.instance.Instance;
import net.minestom.server.instance.block.states.NetherPortal;
import net.minestom.server.utils.BlockPosition;
import net.minestom.server.utils.Position;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
@Setter
public class NetherPortalTeleportEvent extends Event implements CancellableEvent {

    private boolean cancelled = false;
    private final Entity entity;
    private final BlockPosition portalBlockPosition;
    private final NetherPortal portal;
    private final long ticksSpentInPortal;
    private Instance targetInstance;
    private Position targetPosition;
    private NetherPortal targetPortal;
    private boolean createsNewPortal;

    public NetherPortalTeleportEvent(Entity entity, BlockPosition portalBlockPosition, NetherPortal portal, long ticksSpentInPortal, Instance targetInstance, Position targetPosition, NetherPortal targetPortal, boolean createsNewPortal) {
        this.entity = entity;
        this.portalBlockPosition = portalBlockPosition;
        this.portal = portal;
        this.ticksSpentInPortal = ticksSpentInPortal;
        this.targetInstance = targetInstance;
        this.targetPosition = targetPosition;
        this.targetPortal = targetPortal;
        this.createsNewPortal = createsNewPortal;
    }
}