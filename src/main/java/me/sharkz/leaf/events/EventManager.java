package me.sharkz.leaf.events;

import net.minestom.server.MinecraftServer;
import net.minestom.server.event.GlobalEventHandler;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class EventManager {

    private final GlobalEventHandler manager;

    public EventManager() {
        this.manager = MinecraftServer.getGlobalEventHandler();

        /*
        Leaf.getLogger().info("Trying to inject leaf events...");
        long start = System.currentTimeMillis();
        List<LeafEvent<?>> events = inject("me.sharkz.leaf.events", LeafEvent.class);
        events.forEach(leafEvent -> manager.addEventCallback(leafEvent.getSuperClass(), leafEvent::handle));
        Leaf.getLogger().info(events.size() + " events were been injected! (Took " + (System.currentTimeMillis() - start) + " ms)");
         */
    }
}
